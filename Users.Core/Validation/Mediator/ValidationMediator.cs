using System.Reflection;
using FluentValidation;
using FluentValidation.Internal;
using Microsoft.Extensions.DependencyInjection;

namespace Users.Core.Validation.Mediator;
internal class ValidationMediator : IValidationMediator
{
    private readonly IServiceProvider serviceProvider;

    public ValidationMediator(IServiceProvider serviceProvider)
    {
        this.serviceProvider = serviceProvider;
    }

    public void ValidateAndThrow(object obj)
    {
        var objType = obj.GetType();
        var genericValidatorType = typeof(IValidator<>).MakeGenericType(objType);
        var validator = serviceProvider.GetService(genericValidatorType);
        if (validator == null)
        {
            throw new InvalidOperationException("No such validator. Register your validator to DI in Startup.cs.");
        }

        var validationContextType = typeof(ValidationContext<>).MakeGenericType(objType);
        var validationStrategyType = typeof(ValidationStrategy<>).MakeGenericType(objType);
        var delegateType = typeof(Action<>).MakeGenericType(validationStrategyType);
        var throwOnFailuresMethod = GetType().GetMethod(nameof(ThrowOnFailures))!.MakeGenericMethod(objType);
        var @delegate = Delegate.CreateDelegate(delegateType, null, throwOnFailuresMethod);
        var validationContext = validationContextType
            .GetMethod(nameof(ValidationContext<object>.CreateWithOptions))!
            .Invoke(null, new[] { obj, @delegate });

        try
        {
            typeof(IValidator).GetMethod(nameof(IValidator.Validate))!.Invoke(validator, new[] { validationContext });
        }
        catch (TargetInvocationException ex) when (ex.InnerException is FluentValidation.ValidationException fluentValidationException)
        {
            throw new Core.Exceptions.ValidationException(fluentValidationException);
        }
    }

    public static void ThrowOnFailures<T>(ValidationStrategy<T> options)
    {
        options.ThrowOnFailures();
    }
}