using Users.Core.Model;
using Users.Core.Model.Read;

namespace Users.InMemory.Model;
internal class InMemoryUser : IUser
{
    public InMemoryUser(User user)
    {
        Id = user.Id;
        FirstName = user.FirstName;
        LastName = user.LastName;
        Address = user.Address;
        PhoneNumber = user.PhoneNumber;
        DateOfBirth = user.DateOfBirth;
        CreatedAt = user.CreatedAt;
    }
    public InMemoryUser(
        string id,
        string firstName,
        string lastName,
        Address? address,
        string? phoneNumber,
        DateOnly? dateOfBirth,
        DateTime createdAt)
    {
        Id = id;
        FirstName = firstName;
        LastName = lastName;
        Address = address;
        PhoneNumber = phoneNumber;
        DateOfBirth = dateOfBirth;
        CreatedAt = createdAt;
    }

    public string Id { get; }
    public string FirstName { get; }
    public string LastName { get; }
    public Address? Address { get; }
    public string? PhoneNumber { get; }
    public DateOnly? DateOfBirth { get; }
    public DateTime CreatedAt { get; }
}
