using FluentValidation;
using Users.Core.Model;

namespace Users.Core.Validation;
public class AddressValidator : AbstractValidator<Address?>
{
    public AddressValidator()
    {
        RuleFor(add => add!.Value.AddressLine1).NotEmpty();
        RuleFor(add => add!.Value.City).NotEmpty();
        RuleFor(add => add!.Value.RegionCode)
            .SetValidator(new LocationCodeValidator())
            .NotEmpty();
        RuleFor(add => add!.Value.ZipCode)
            .SetValidator(new ZipCodeValidator())
            .NotEmpty();
        RuleFor(add => add!.Value.CountryCode)
            .SetValidator(new LocationCodeValidator())
            .NotEmpty();
    }
}