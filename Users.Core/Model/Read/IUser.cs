namespace Users.Core.Model.Read;
public interface IUser
{
    string Id { get; }
    string FirstName { get; }
    string LastName { get; }
    Address? Address { get; }
    string? PhoneNumber { get; }
    DateOnly? DateOfBirth { get; }
    DateTime CreatedAt { get; }
}