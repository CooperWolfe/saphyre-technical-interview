FROM mcr.microsoft.com/dotnet/sdk:7.0 AS base
WORKDIR /src
COPY Users.Service/Users.Service.csproj Users.Service/
COPY Users.Core/Users.Core.csproj Users.Core/
COPY Users.InMemory/Users.InMemory.csproj Users.InMemory/
COPY Users.Blazor/Users.Blazor.csproj Users.Blazor/
RUN dotnet restore Users.Service
COPY Users.Service/ Users.Service/
COPY Users.Core/ Users.Core/
COPY Users.InMemory/ Users.InMemory/
COPY Users.Blazor/ Users.Blazor/


FROM base AS debug
ENV ASPNETCORE_ENVIRONMENT Development
ENV DetailedErrors true
WORKDIR /src/Users.Service
RUN dotnet build -c Debug --no-restore
ENTRYPOINT [ "dotnet", "bin/Debug/net7.0/Users.Service.dll" ]


FROM base as build
WORKDIR /src
ARG BUILD_NUMBER
RUN dotnet build Users.Service/ -c Release --no-restore -p:Version="${BUILD_NUMBER}"


FROM build AS publish
WORKDIR /src
RUN dotnet publish Users.Service/ -c Release -o /app/publish --no-restore --no-build


FROM mcr.microsoft.com/dotnet/aspnet:7.0-alpine AS release
RUN apk add bash
RUN addgroup -g 10001 -S saphyre && adduser -u 10001 -S saphyre -G saphyre
USER saphyre
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT [ "dotnet", "Users.Service.dll" ]
