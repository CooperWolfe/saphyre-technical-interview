namespace Users.Blazor.ViewModel;
public class UserFormModel
{
    public UserFormModel()
    {
    }
    public UserFormModel(UserReadModel user)
    {
        Id = user.Id;
        FirstName = user.FirstName;
        LastName = user.LastName;
        HasAddress = user.Address != null;
        AddressLine1 = user.Address?.AddressLine1;
        AddressLine2 = user.Address?.AddressLine2;
        City = user.Address?.City;
        RegionCode = user.Address?.RegionCode;
        ZipCode = user.Address?.ZipCode;
        CountryCode = user.Address?.CountryCode;
        PhoneNumber = user.PhoneNumber;
        DateOfBirth = user.DateOfBirth;
    }

    public string? Id { get; set; }
    public string? FirstName { get; set; }
    public string? LastName { get; set; }
    public bool HasAddress { get; set; }
    public string? AddressLine1 { get; set; }
    public string? AddressLine2 { get; set; }
    public string? City { get; set; }
    public string? RegionCode { get; set; }
    public string? CountryCode { get; set; }
    public string? ZipCode { get; set; }
    public string? PhoneNumber { get; set; }
    public DateOnly? DateOfBirth { get; set; }

    public bool IsCreating() => Id == null;
}