﻿using Users.Core.Repositories;
using Users.InMemory;

namespace Microsoft.Extensions.DependencyInjection;
public static class InMemoryServiceCollectionExtensions
{
    public static void AddInMemory(this IServiceCollection services)
    {
        services.AddSingleton<InMemoryUserRepository>();
        services.AddSingleton<IUserRepository>(sp => new ProcessSimulatingUserRepository(sp.GetRequiredService<InMemoryUserRepository>()));
    }
}
