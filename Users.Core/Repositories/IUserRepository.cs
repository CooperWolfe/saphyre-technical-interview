using Users.Core.Model;
using Users.Core.Model.Read;

namespace Users.Core.Repositories;
public interface IUserRepository
{
    Task Create(User user, CancellationToken cancellationToken);
    Task Delete(string id, CancellationToken cancellationToken);
    Task<IUser?> GetUser(string id, CancellationToken cancellationToken);
    Task<IEnumerable<IUser>> GetUsers(CancellationToken cancellationToken);
    Task Update(User user, CancellationToken cancellationToken);
}