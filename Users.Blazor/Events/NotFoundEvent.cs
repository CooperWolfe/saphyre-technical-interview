using Users.Core.Exceptions;

namespace Users.Blazor.Events;
public class NotFoundEvent
{
    public NotFoundEvent(string id, NotFoundException exception)
    {
        Id = id;
        Exception = exception;
    }

    public string Id { get; }
    public NotFoundException Exception { get; }
}