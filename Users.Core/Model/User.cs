using Users.Core.Commands;
using Users.Core.Model.Read;

namespace Users.Core.Model;
public class User
{
    private User(
        string firstName,
        string lastName,
        Address? address,
        string? phoneNumber,
        DateOnly? dateOfBirth)
    {
        Id = Guid.NewGuid().ToString();
        FirstName = firstName;
        LastName = lastName;
        Address = address;
        PhoneNumber = phoneNumber;
        DateOfBirth = dateOfBirth;
        CreatedAt = DateTime.UtcNow;
    }
    internal User(IUser user)
    {
        Id = user.Id;
        FirstName = user.FirstName;
        LastName = user.LastName;
        Address = user.Address;
        PhoneNumber = user.PhoneNumber;
        DateOfBirth = user.DateOfBirth;
        CreatedAt = user.CreatedAt;
    }

    public string Id { get; }
    public string FirstName { get; private set; }
    public string LastName { get; private set; }
    public Address? Address { get; private set; }
    public string? PhoneNumber { get; private set; }
    public DateOnly? DateOfBirth { get; private set; }
    public DateTime CreatedAt { get; }

    internal static User Create(CreateUserCommand command)
    {
        return new User(
            firstName: command.FirstName,
            lastName: command.LastName,
            address: command.Address,
            phoneNumber: command.PhoneNumber,
            dateOfBirth: command.DateOfBirth);
    }

    internal void Update(UpdateUserCommand command)
    {
        FirstName = command.FirstName;
        LastName = command.LastName;
        Address = command.Address;
        PhoneNumber = command.PhoneNumber;
        DateOfBirth = command.DateOfBirth;
    }
}