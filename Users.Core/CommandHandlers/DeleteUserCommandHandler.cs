using MediatR;
using Microsoft.Extensions.Logging;
using Users.Core.Commands;
using Users.Core.Repositories;

namespace Users.Core.CommandHandlers;
internal class DeleteUserCommandHandler : IRequestHandler<DeleteUserCommand>
{
    private readonly ILogger<DeleteUserCommandHandler> logger;
    private readonly IUserRepository userRepository;

    public DeleteUserCommandHandler(
        ILogger<DeleteUserCommandHandler> logger,
        IUserRepository userRepository)
    {
        this.logger = logger;
        this.userRepository = userRepository;
    }

    public async Task<Unit> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
    {
        logger.LogDebug($"Deleting user with ID {request.Id}");
        await userRepository.Delete(request.Id, cancellationToken);
        return Unit.Value;
    }
}