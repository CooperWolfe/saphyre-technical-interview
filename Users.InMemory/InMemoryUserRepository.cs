using Users.Core.Model;
using Users.Core.Model.Read;
using Users.Core.Repositories;
using Users.InMemory.Model;

namespace Users.InMemory;
internal class InMemoryUserRepository : IUserRepository
{
    private Dictionary<string, InMemoryUser> users;

    public InMemoryUserRepository()
    {
        users = new Dictionary<string, InMemoryUser>
        {
            { "abc123", new InMemoryUser(
                id: "abc123",
                firstName: "Luffy",
                lastName: "Monkey",
                address: new Core.Model.Address(
                    addressLine1: "123 Curly Dadan St.",
                    addressLine2: "In the forest",
                    city: "Dawn Island",
                    regionCode: "DI",
                    countryCode: "EB",
                    zipCode: "12345"),
                phoneNumber: "18008675309",
                dateOfBirth: new DateOnly(
                    year: 2003,
                    month: 5,
                    day: 5),
                createdAt: DateTime.UtcNow) },
            { "xyz321", new InMemoryUser(
                id: "xyz321",
                firstName: "Zoro",
                lastName: "Roronoa",
                address: new Core.Model.Address(
                    addressLine1: "321 Shimotsuki St.",
                    addressLine2: null,
                    city: "Shimotsuki Village",
                    regionCode: "SV",
                    countryCode: "EB",
                    zipCode: "54321"),
                phoneNumber: "18009035768",
                dateOfBirth: new DateOnly(
                    year: 2001,
                    month: 11,
                    day: 11),
                createdAt: DateTime.UtcNow) },
        };
    }

    public Task Create(User user, CancellationToken cancellationToken)
    {
        users[user.Id] = new InMemoryUser(user);
        return Task.CompletedTask;
    }

    public Task Delete(string id, CancellationToken cancellationToken)
    {
        users.Remove(id);
        return Task.CompletedTask;
    }

    public Task<IUser?> GetUser(string id, CancellationToken cancellationToken)
    {
        users.TryGetValue(id, out InMemoryUser? user);
        return Task.FromResult<IUser?>(user);
    }

    public Task<IEnumerable<IUser>> GetUsers(CancellationToken cancellationToken)
    {
        return Task.FromResult<IEnumerable<IUser>>(users.Values);
    }

    public Task Update(User user, CancellationToken cancellationToken)
    {
        users[user.Id] = new InMemoryUser(user);
        return Task.CompletedTask;
    }
}