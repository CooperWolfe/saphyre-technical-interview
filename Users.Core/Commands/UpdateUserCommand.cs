using MediatR;
using Users.Core.Model;

namespace Users.Core.Commands;
public class UpdateUserCommand : IRequest
{
    public UpdateUserCommand(string id, string firstName, string lastName, Address? address, string? phoneNumber, DateOnly? dateOfBirth)
    {
        Id = id;
        FirstName = firstName;
        LastName = lastName;
        Address = address;
        PhoneNumber = phoneNumber;
        DateOfBirth = dateOfBirth;
    }

    public string Id { get; }
    public string FirstName { get; }
    public string LastName { get; }
    public Address? Address { get; }
    public string? PhoneNumber { get; }
    public DateOnly? DateOfBirth { get; }
}