namespace Users.Core.Commands.Responses;
public class CreateUserResponse
{
    internal CreateUserResponse(string createdUserId)
    {
        CreatedUserId = createdUserId;
    }

    public string CreatedUserId { get; }
}