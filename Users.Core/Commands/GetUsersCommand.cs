using MediatR;
using Users.Core.Commands.Responses;

namespace Users.Core.Commands;
public class GetUsersCommand : IRequest<GetUsersResponse>
{
}