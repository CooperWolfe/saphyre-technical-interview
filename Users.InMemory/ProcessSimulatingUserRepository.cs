using Users.Core.Model;
using Users.Core.Model.Read;
using Users.Core.Repositories;

namespace Users.InMemory;
internal class ProcessSimulatingUserRepository : IUserRepository
{
    private readonly IUserRepository userRepository;

    public ProcessSimulatingUserRepository(IUserRepository userRepository)
    {
        this.userRepository = userRepository;
    }

    public async Task Create(User user, CancellationToken cancellationToken)
    {
        await Task.Delay(1000, cancellationToken);
        await userRepository.Create(user, cancellationToken);
    }

    public async Task Delete(string id, CancellationToken cancellationToken)
    {
        await Task.Delay(1000, cancellationToken);
        await userRepository.Delete(id, cancellationToken);
    }

    public async Task<IUser?> GetUser(string id, CancellationToken cancellationToken)
    {
        await Task.Delay(1000, cancellationToken);
        return await userRepository.GetUser(id, cancellationToken);
    }

    public async Task<IEnumerable<IUser>> GetUsers(CancellationToken cancellationToken)
    {
        await Task.Delay(1000, cancellationToken);
        return await userRepository.GetUsers(cancellationToken);
    }

    public async Task Update(User user, CancellationToken cancellationToken)
    {
        await Task.Delay(1000, cancellationToken);
        await userRepository.Update(user, cancellationToken);
    }
}