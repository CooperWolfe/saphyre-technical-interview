namespace Users.Core.Exceptions;
[Serializable]
public class UsersException : Exception
{
    public UsersException() { }
    public UsersException(string message) : base(message) { }
    public UsersException(string message, Exception inner) : base(message, inner) { }
}