namespace Users.Core.Model;
public struct Address
{
    public Address(
        string addressLine1,
        string? addressLine2,
        string city,
        string regionCode,
        string countryCode,
        string zipCode)
    {
        AddressLine1 = addressLine1;
        AddressLine2 = addressLine2;
        City = city;
        RegionCode = regionCode;
        CountryCode = countryCode;
        ZipCode = zipCode;
    }

    public string AddressLine1 { get; }
    public string? AddressLine2 { get; }
    public string City { get; }
    public string RegionCode { get; }
    public string CountryCode { get; }
    public string ZipCode { get; }
}