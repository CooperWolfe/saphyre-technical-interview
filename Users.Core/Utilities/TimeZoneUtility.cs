namespace Users.Core.Utilities;
internal static class TimeZoneUtility
{
    public static readonly TimeSpan MaxTimeZoneOffset = TimeSpan.FromHours(14);
    public static readonly TimeSpan MaxAllowedClockSkew = TimeSpan.FromMinutes(10);

    public static DateOnly MaxToday()
    {
        return DateOnly.FromDateTime(DateTime.UtcNow + MaxTimeZoneOffset + MaxAllowedClockSkew);
    }
}