﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

void Configure(IServiceCollection services)
{
    services.AddUsersCore();
    services.AddInMemory();
}

var blazorHost = Users.Blazor.HostBuilderFactory.Create(args, Configure)
    .UseContentRoot(".")
    .Build();
await blazorHost.RunAsync();