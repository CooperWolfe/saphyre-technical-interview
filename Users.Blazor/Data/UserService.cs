using MediatR;
using Users.Core.Commands.Responses;
using Users.Core.Commands;

namespace Users.Blazor.Data;

public class UserService
{
    private readonly IMediator mediator;

    public UserService(IMediator mediator)
    {
        this.mediator = mediator;
    }

    public Task<GetUserResponse> GetUserAsync(GetUserCommand command, CancellationToken cancellationToken)
        => mediator.Send(command, cancellationToken);
    public Task<GetUsersResponse> GetUsersAsync(GetUsersCommand command, CancellationToken cancellationToken)
        => mediator.Send(command, cancellationToken);
    public Task<CreateUserResponse> CreateUserAsync(CreateUserCommand command, CancellationToken cancellationToken)
        => mediator.Send(command, cancellationToken);
    public Task UpdateUserAsync(UpdateUserCommand command, CancellationToken cancellationToken)
        => mediator.Send(command, cancellationToken);
    public Task DeleteUserAsync(DeleteUserCommand command, CancellationToken cancellationToken)
        => mediator.Send(command, cancellationToken);
}
