namespace Users.Blazor;

public static class HostBuilderFactory
{
    public static IHostBuilder Create(string[] args, Action<IServiceCollection> configureDelegate) =>
        Host.CreateDefaultBuilder(args)
            .ConfigureServices(configureDelegate)
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<Startup>();
            });
}