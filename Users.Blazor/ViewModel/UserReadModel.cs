using Users.Core.Model;
using Users.Core.Model.Read;

namespace Users.Blazor.ViewModel;
public class UserReadModel
{
    public UserReadModel(IUser user)
    {
        Id = user.Id;
        FirstName = user.FirstName;
        LastName = user.LastName;
        Address = user.Address;
        PhoneNumber = user.PhoneNumber;
        DateOfBirth = user.DateOfBirth;
    }
    public UserReadModel(
        string id,
        string firstName,
        string lastName,
        Address? address,
        string? phoneNumber,
        DateOnly? dateOfBirth)
    {
        Id = id;
        FirstName = firstName;
        LastName = lastName;
        Address = address;
        PhoneNumber = phoneNumber;
        DateOfBirth = dateOfBirth;
    }

    public string Id { get; }
    public string FirstName { get; }
    public string LastName { get; }
    public Address? Address { get; }
    public string? PhoneNumber { get; }
    public DateOnly? DateOfBirth { get; }
    public uint? Age
    {
        get
        {
            if (!DateOfBirth.HasValue) return null;
            uint now = uint.Parse(DateTime.Today.ToString("yyyyMMdd"));
            uint dob = uint.Parse(DateOfBirth.Value.ToString("yyyyMMdd"));
            uint age = (uint)((now - dob) * 1e-4);
            return age;
        }
    }
}