using Users.Core.Model.Read;

namespace Users.Core.Commands.Responses;
public class GetUserResponse
{
    internal GetUserResponse(IUser? user)
    {
        User = user;
    }

    public IUser? User { get; }
}