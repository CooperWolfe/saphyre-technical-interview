using MediatR;
using Microsoft.Extensions.Logging;
using Users.Core.Commands;
using Users.Core.Commands.Responses;
using Users.Core.Repositories;

namespace Users.Core.CommandHandlers;
internal class GetUsersCommandHandler : IRequestHandler<GetUsersCommand, GetUsersResponse>
{
    private readonly ILogger<GetUsersCommandHandler> logger;
    private readonly IUserRepository userRepository;

    public GetUsersCommandHandler(
        ILogger<GetUsersCommandHandler> logger,
        IUserRepository userRepository)
    {
        this.logger = logger;
        this.userRepository = userRepository;
    }

    public async Task<GetUsersResponse> Handle(GetUsersCommand request, CancellationToken cancellationToken)
    {
        logger.LogDebug("Getting users");
        var users = await userRepository.GetUsers(cancellationToken);
        return new GetUsersResponse(users);
    }
}
