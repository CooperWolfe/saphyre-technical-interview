using MediatR;
using Microsoft.Extensions.Logging;
using Users.Core.Commands;
using Users.Core.Exceptions;
using Users.Core.Model;
using Users.Core.Repositories;
using Users.Core.Validation.Mediator;

namespace Users.Core.CommandHandlers;
internal class UpdateUserCommandHandler : IRequestHandler<UpdateUserCommand>
{
    private readonly ILogger<UpdateUserCommandHandler> logger;
    private readonly IUserRepository userRepository;
    private readonly IValidationMediator validator;

    public UpdateUserCommandHandler(
        ILogger<UpdateUserCommandHandler> logger,
        IUserRepository userRepository,
        IValidationMediator validator)
    {
        this.logger = logger;
        this.userRepository = userRepository;
        this.validator = validator;
    }

    public async Task<Unit> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
    {
        validator.ValidateAndThrow(request);

        logger.LogDebug($"Getting user by ID {request.Id}");
        var iuser = await userRepository.GetUser(id: request.Id, cancellationToken);
        if (iuser == null)
        {
            throw new NotFoundException("The requested user was not found");
        }

        logger.LogDebug("Updating user");
        var user = new User(iuser);
        user.Update(request);
        await userRepository.Update(user, cancellationToken);

        return Unit.Value;
    }
}
