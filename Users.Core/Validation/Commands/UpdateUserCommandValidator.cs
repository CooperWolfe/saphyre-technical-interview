using FluentValidation;
using Users.Core.Commands;
using Users.Core.Utilities;

namespace Users.Core.Validation.Commands;
internal class UpdateUserCommandValidator : AbstractValidator<UpdateUserCommand>
{
    public UpdateUserCommandValidator()
    {
        RuleFor(cmd => cmd.FirstName).NotEmpty();
        RuleFor(cmd => cmd.LastName).NotEmpty();
        RuleFor(cmd => cmd.Address).SetValidator(new AddressValidator());
        RuleFor(cmd => cmd.PhoneNumber!).SetValidator(new PhoneNumberValidator());
        RuleFor(cmd => cmd.DateOfBirth).SetValidator(new DateOfBirthValidator(today: TimeZoneUtility.MaxToday()));
    }
}