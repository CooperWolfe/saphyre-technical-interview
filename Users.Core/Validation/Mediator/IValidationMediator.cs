namespace Users.Core.Validation.Mediator;
public interface IValidationMediator
{
    /// <throws cref="Core.Exceptions.ValidationException" />
    void ValidateAndThrow(object obj);
}