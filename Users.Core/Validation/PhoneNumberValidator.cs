using System.Text.RegularExpressions;
using FluentValidation;

namespace Users.Core.Validation;
public class PhoneNumberValidator : AbstractValidator<string>
{
    public PhoneNumberValidator()
    {
        RuleFor(num => num)
            .Must(num => Regex.IsMatch(num, @"^\d{11,13}$"))
            .WithMessage("Must be a valid 11-13 digit phone number");
    }
}