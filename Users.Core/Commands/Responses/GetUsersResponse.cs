using Users.Core.Model.Read;

namespace Users.Core.Commands.Responses;
public class GetUsersResponse
{
    internal GetUsersResponse(IEnumerable<IUser> users)
    {
        Users = users;
    }

    public IEnumerable<IUser> Users { get; }
}