using System.Text.RegularExpressions;
using FluentValidation;

namespace Users.Core.Validation;
public class ZipCodeValidator : AbstractValidator<string>
{
    public ZipCodeValidator()
    {
        RuleFor(code => code)
            .Must(code => Regex.IsMatch(code, @"(^\d{5}$)|(^\d{9}$)|(^\d{5}-\d{4}$)"))
            .WithMessage("Invalid zip code");
    }
}