using MediatR;
using Users.Core.Commands.Responses;

namespace Users.Core.Commands;
public class GetUserCommand : IRequest<GetUserResponse>
{
    public GetUserCommand(string id)
    {
        Id = id;
    }

    public string Id { get; }
}