# Blazor PoC

## Project Architecture
I structured this project in a [hexagonal architecture](https://en.wikipedia.org/wiki/Hexagonal_architecture_(software)). I typically prefer this architecture as it allows for the easiest addition and swapping out of supporting technologies. For example, the Users.InMemory project could be easily swapped out for a Users.Postgres; and a Users.GraphQL could be easily mixed in if the need arose.
- Users.Blazor contains all the front-end code and the service layer.
- Users.Core contains the core business logic and entities. It defines the interfaces supporting projects will implement.
- Users.InMemory implements the interfaces used by Users.Core via in-memory storage.
- Users.Service combines the projects together into a single executable.

## Room for improvement

### Transactions
This project does not take consider the [ACID](https://en.wikipedia.org/wiki/Atomicity_(database_systems))-ity of its transactions. Employing the [unit of work pattern](https://learn.microsoft.com/en-us/aspnet/mvc/overview/older-versions/getting-started-with-ef-5-using-mvc-4/implementing-the-repository-and-unit-of-work-patterns-in-an-asp-net-mvc-application) would be a great way to improve, for example, user updates. Combined with dependency injection, this could be done with no changes to existing files.

### Entity Versioning
The project as-is will allow a synchronous user to override your changes without ever seeing them. Entity versioning would prevent this: a version field would be added to the user entity, all requests to modify the entity would present the version field, and an exception would be thrown and handled on a mismatch.

### Persistence
For the sake of time, I put the storage layer in-memory. Because of the hexagonal architecture, swapping this out for a real database would be a breeze.

### Localization
Depending on the deployment expectations of this hypothetical application, localization may have been a necessary feature. However, I did not make use of .NET's [resource files](https://learn.microsoft.com/en-us/dotnet/core/extensions/localization#resource-files), so adding localization would require refactor.