using MediatR;
using Users.Core.Commands.Responses;
using Users.Core.Model;

namespace Users.Core.Commands;
public class CreateUserCommand : IRequest<CreateUserResponse>
{
    public CreateUserCommand(string firstName, string lastName, Address? address, string? phoneNumber, DateOnly? dateOfBirth)
    {
        FirstName = firstName;
        LastName = lastName;
        Address = address;
        PhoneNumber = phoneNumber;
        DateOfBirth = dateOfBirth;
    }

    public string FirstName { get; }
    public string LastName { get; }
    public Address? Address { get; }
    public string? PhoneNumber { get; }
    public DateOnly? DateOfBirth { get; }
}