namespace Users.Core.Exceptions;
public class ValidationException : UsersException
{
    public ValidationException(FluentValidation.ValidationException fluentValidationException) : base(fluentValidationException.Message, fluentValidationException)
    {
        FluentValidationException = fluentValidationException;
    }

    public FluentValidation.ValidationException FluentValidationException { get; }
}