using Microsoft.AspNetCore.Components;

namespace Users.Blazor.Shared;
public class CancellableComponent : ComponentBase, IDisposable
{
    private CancellationTokenSource cts = new();

    protected CancellationToken CancellationToken => cts.Token;

    protected void Cancel()
    {
        cts.Cancel();
        cts.Dispose();
        cts = new();
    }

    public virtual void Dispose()
    {
        cts.Cancel();
        cts.Dispose();
    }
}
