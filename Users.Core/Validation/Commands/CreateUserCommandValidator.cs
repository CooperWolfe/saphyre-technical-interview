using FluentValidation;
using Users.Core.Commands;
using Users.Core.Utilities;

namespace Users.Core.Validation.Commands;
internal class CreateUserCommandValidator : AbstractValidator<CreateUserCommand>
{
    public CreateUserCommandValidator()
    {        
        RuleFor(cmd => cmd.FirstName).NotEmpty();
        RuleFor(cmd => cmd.LastName).NotEmpty();
        RuleFor(cmd => cmd.Address).SetValidator(new AddressValidator());
        RuleFor(cmd => cmd.DateOfBirth).SetValidator(new DateOfBirthValidator(today: TimeZoneUtility.MaxToday()));
        RuleFor(cmd => cmd.PhoneNumber!).SetValidator(new PhoneNumberValidator());
    }
}