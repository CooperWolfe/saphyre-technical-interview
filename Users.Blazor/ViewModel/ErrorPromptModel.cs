namespace Users.Blazor.ViewModel;
public class ErrorPromptModel
{
    public ErrorPromptModel(Exception exception, string prompt, Action? onDismiss = null)
    {
        Exception = exception;
        Prompt = prompt;
        OnDismiss = onDismiss;
    }

    public Exception Exception { get; }
    public string Prompt { get; }
    public Action? OnDismiss { get; }
}