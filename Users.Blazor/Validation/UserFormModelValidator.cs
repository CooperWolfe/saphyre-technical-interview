using FluentValidation;
using Users.Blazor.ViewModel;
using Users.Core.Validation;

namespace Users.Blazor.Validation;
internal class UserFormModelValidator : AbstractValidator<UserFormModel>
{
    public UserFormModelValidator()
    {
        RuleFor(model => model.FirstName).NotEmpty();
        RuleFor(model => model.LastName).NotEmpty();
        RuleFor(model => model.PhoneNumber!).SetValidator(new PhoneNumberValidator());
        RuleFor(model => model.DateOfBirth)
            .SetValidator(new DateOfBirthValidator(DateOnly.FromDateTime(DateTime.Today)));
        When(model => model.HasAddress, () =>
        {
            RuleFor(model => model.AddressLine1).NotEmpty();
            RuleFor(model => model.City).NotEmpty();
            RuleFor(model => model.RegionCode!)
                .SetValidator(new LocationCodeValidator())
                .NotEmpty();
            RuleFor(model => model.ZipCode!)
                .SetValidator(new ZipCodeValidator())
                .NotEmpty();
            RuleFor(model => model.CountryCode!)
                .SetValidator(new LocationCodeValidator())
                .NotEmpty();
        });
    }
}