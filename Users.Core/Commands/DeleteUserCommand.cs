using MediatR;

namespace Users.Core.Commands;
public class DeleteUserCommand : IRequest
{
    public DeleteUserCommand(string id)
    {
        Id = id;
    }

    public string Id { get; }
}