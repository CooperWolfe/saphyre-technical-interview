using MediatR;
using Microsoft.Extensions.Logging;
using Users.Core.Commands;
using Users.Core.Commands.Responses;
using Users.Core.Model;
using Users.Core.Repositories;
using Users.Core.Validation.Mediator;

namespace Users.Core.CommandHandlers;
internal class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, CreateUserResponse>
{
    private readonly ILogger<CreateUserCommandHandler> logger;
    private readonly IUserRepository userRepository;
    private readonly IValidationMediator validator;

    public CreateUserCommandHandler(
        ILogger<CreateUserCommandHandler> logger,
        IUserRepository userRepository,
        IValidationMediator validator)
    {
        this.logger = logger;
        this.userRepository = userRepository;
        this.validator = validator;
    }

    public async Task<CreateUserResponse> Handle(CreateUserCommand request, CancellationToken cancellationToken)
    {
        validator.ValidateAndThrow(request);

        logger.LogDebug($"Creating user");
        var user = User.Create(request);
        await userRepository.Create(user, cancellationToken);
        return new CreateUserResponse(user.Id);
    }
}
