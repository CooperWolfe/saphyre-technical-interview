using FluentValidation;

namespace Users.Core.Validation;
public class DateOfBirthValidator : AbstractValidator<DateOnly?>
{
    /// <param name="now">The current date (to compare to). Consider time zone.</param>
    public DateOfBirthValidator(DateOnly today)
    {
        RuleFor(dob => dob)
            .LessThanOrEqualTo(today)
            .WithMessage("Must be on or before today");
    }
}