using System.Text.RegularExpressions;
using FluentValidation;

namespace Users.Core.Validation;
public class LocationCodeValidator : AbstractValidator<string>
{
    public LocationCodeValidator()
    {
        RuleFor(code => code)
            .Must(code => Regex.IsMatch(code, @"^[a-zA-Z]{2}$"))
            .WithMessage("Must be exactly two alphabetic characters");
    }
}