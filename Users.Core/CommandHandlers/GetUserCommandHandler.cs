using MediatR;
using Microsoft.Extensions.Logging;
using Users.Core.Commands;
using Users.Core.Commands.Responses;
using Users.Core.Repositories;

namespace Users.Core.CommandHandlers;
internal class GetUserCommandHandler : IRequestHandler<GetUserCommand, GetUserResponse>
{
    private readonly ILogger<GetUserCommandHandler> logger;
    private readonly IUserRepository userRepository;

    public GetUserCommandHandler(
        ILogger<GetUserCommandHandler> logger,
        IUserRepository userRepository)
    {
        this.logger = logger;
        this.userRepository = userRepository;
    }

    public async Task<GetUserResponse> Handle(GetUserCommand request, CancellationToken cancellationToken)
    {
        logger.LogDebug($"Getting user with ID {request.Id}");
        var user = await userRepository.GetUser(request.Id, cancellationToken);
        return new GetUserResponse(user);
    }
}
