using FluentValidation;
using MediatR;
using Users.Core.Commands;
using Users.Core.Validation.Commands;
using Users.Core.Validation.Mediator;

namespace Microsoft.Extensions.DependencyInjection;
public static class UsersCoreServiceCollectionExtensions
{
    public static void AddUsersCore(this IServiceCollection services)
    {
        services.AddMediatR(typeof(UsersCoreServiceCollectionExtensions));
        services.AddSingleton<IValidationMediator, ValidationMediator>();
        services.AddTransient<IValidator<CreateUserCommand>, CreateUserCommandValidator>();
        services.AddTransient<IValidator<UpdateUserCommand>, UpdateUserCommandValidator>();
    }
}