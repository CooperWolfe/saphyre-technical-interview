using Microsoft.AspNetCore.Components.Forms;

namespace Users.Blazor.Shared;
internal class BootstrapValidationFieldCssClassProvider : FieldCssClassProvider
{
    public override string GetFieldCssClass(EditContext editContext, in FieldIdentifier fieldIdentifier)
    {
        if (editContext == null)
        {
            throw new ArgumentNullException(nameof(editContext));
        }

        bool isValid = !editContext.GetValidationMessages(fieldIdentifier).Any();

        if (editContext.IsModified(fieldIdentifier))
        {
            return isValid ? "" : "is-invalid";
        }

        return isValid ? string.Empty : "is-invalid";
    }
}